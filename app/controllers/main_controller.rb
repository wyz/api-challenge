class MainController < ApplicationController
  def index
    @api_object = ApiFormObject.new
  end

  def show
    offers_api = SponsorPay::Api::Offers.new(SponsorPay::Api::DEAFULT_OPTIONS.merge(offer_params))
    @offers = offers_api.offers

    render text: "no offers" if @offers.empty?
  end

  private
  def offer_params
    params.require(:api_form_object).permit(:uid, :pub0, :page)
  end
end