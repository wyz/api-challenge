class ApiFormObject
  extend ActiveModel::Naming
  include ActiveModel::Conversion
  include Virtus

  attribute :uid, String
  attribute :pub0, String
  attribute :page, String

  def persisted?
    false
  end
end