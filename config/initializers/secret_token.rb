# Be sure to restart your server when you modify this file.

# Your secret key is used for verifying the integrity of signed cookies.
# If you change this key, all old signed cookies will become invalid!

# Make sure the secret is at least 30 characters and all random,
# no regular words or you'll be exposed to dictionary attacks.
# You can use `rake secret` to generate a secure secret key.

# Make sure your secret_key_base is kept private
# if you're sharing your code publicly.
ApiChallenge::Application.config.secret_key_base = 'a8554b5381f4ccec06874dfc16a5f5bfb7b1782f62eb37d2c0f125cd26ab176d06a61247ec513258203f26e1115868a3b107a30a1faf610e97e021a854d3dbbb'
