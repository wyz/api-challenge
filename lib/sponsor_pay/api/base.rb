class SponsorPay::Api::Base
  BASE_URL = "http://api.sponsorpay.com/feed/#{SponsorPay::Api::VERSION}"
  attr_reader :options, :format

  def initialize(options = {})
    @format = options.delete(:format)
    @options = options.symbolize_keys
  end


  private
  def ordered_params
    result = []
    options.keys.sort.each do |key|
      result << "#{key}=#{options[key]}"
    end

    result.join("&")
  end

  def hashed_signature
    Digest::SHA1.hexdigest [ordered_params,SponsorPay::Api::KEY].join("&")
  end

  def request_param_string
    opts  = options.merge(hashkey: hashed_signature)
    pairs = opts.each.inject([]){ |res, (key, value)| res<<"#{key}=#{value}" }
    pairs.join("&")
  end

  def signature(response)
    response.headers.fetch("x-sponsorpay-response-signature", nil)
  rescue
    nil
  end

  def valid_response?(response)
    response.code == 200 && signature(response)
  end
end