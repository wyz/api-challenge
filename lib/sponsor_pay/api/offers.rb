module SponsorPay::Api
  class Offers < Base
    URL = [BASE_URL, "offers"].join("/")

    def get_offers_response
      @response ||= HTTParty.get(url)
    end

    def offers
      response = get_offers_response
      if valid_response?(response)
        response["offers"]
      else
        []
      end
    end

    def url
      base = format ? [URL,format].join(".") : URL
      [base, request_param_string].join("?")
    end
  end
end