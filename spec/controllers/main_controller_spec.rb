require "spec_helper"

describe MainController do
  describe "show" do
    let(:params) { {uid: "uid", page: 1} }
    let(:perform) { get :show, api_form_object: params }

    before do
      headers = {
          "content-type" => ["application/json; charset=utf-8"],
          "x-sponsorpay-response-signature" => "something"
      }
      url = SponsorPay::Api::Offers.new(SponsorPay::Api::DEAFULT_OPTIONS.merge(params)).url
      stub_request(:get, url).to_return(body: mocked_response.to_json, :headers => headers)
    end
    context "offers present" do
      let(:mocked_response) { {"offers" => [1, 2, 3]} }
      it "renders the show template" do
        perform

        response.should render_template("show")
      end
      it "loads the offers" do
        perform

        assigns[:offers].should =~ [1, 2, 3]
      end
    end
    context "offers absent" do
      let(:mocked_response) { {"offers" => []} }
      it "renders a message if no offers" do
        perform

        response.body.should == "no offers"
      end
    end
  end
end