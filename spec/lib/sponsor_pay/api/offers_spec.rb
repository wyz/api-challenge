require "spec_helper"

describe SponsorPay::Api::Offers do
  let(:offers_api) { described_class.new }

  describe "#url" do
    it "should return a url with correct base" do
      offers_api.url.should match(SponsorPay::Api::Base::BASE_URL)
    end

    it "should return a url with jormat if required" do
      described_class.new({format: "json"}).url.should match("offers.json")
    end

    it "should contain a hashkey" do
      offers_api.url.should match("hashkey=")
    end
  end

  describe "#get_offers_response" do
    let(:mocked_response) { {"offers" => [1, 2, 3]} }
    let(:headers) { {"content-type" => ["application/json; charset=utf-8"]} }
    before do
      stub_request(:get, offers_api.url).to_return(body: mocked_response.to_json, :headers => headers)
    end

    it "should return a http response" do
      offers_api.get_offers_response.parsed_response.should == mocked_response
    end
  end

  describe "#offers" do
    let(:mocked_response) { {"offers" => [1, 2, 3]} }
    let(:headers) { {"content-type" => ["application/json; charset=utf-8"]} }


    context "signature present" do
      let(:signature) { {"x-sponsorpay-response-signature" => "something"} }

      context "code is not 200" do
        let(:code) { 400 }

        it "should be empty" do
          stub_request(:get, offers_api.url).to_return(body: mocked_response.to_json, :headers => headers.merge(signature), :status => code)

          offers_api.offers.should be_empty
        end
      end

      context "code is 200" do
        let(:code) { 200 }

        it "should be empty" do
          stub_request(:get, offers_api.url).to_return(body: mocked_response.to_json, :headers => headers.merge(signature), :status => code)

          offers_api.offers.should =~ [1, 2, 3]
        end
      end
    end
  end
end